﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnCollide : MonoBehaviour
{
    public Transform colliderToEnterToWin;
    public int sceneId;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == colliderToEnterToWin.gameObject)
        {
            SceneManager.LoadScene(sceneId, LoadSceneMode.Single);
        }
    }
}
