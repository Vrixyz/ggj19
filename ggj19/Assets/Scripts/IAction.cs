﻿using UnityEngine;

public abstract class IAction : MonoBehaviour
{
    public abstract void Fire();
}