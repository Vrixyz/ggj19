﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverOnCollide : MonoBehaviour
{
    public int sceneId;

    private Transform colliderToEnterToWin;
    // Start is called before the first frame update
    void Start()
    {
        colliderToEnterToWin = GameObject.FindWithTag("Player").GetComponent<Transform>();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == colliderToEnterToWin.gameObject)
        {
            SceneManager.LoadScene(sceneId, LoadSceneMode.Single);
        }
    }
}
