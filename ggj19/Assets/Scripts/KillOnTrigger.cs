﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class KillOnTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("KillOnTrigger Start");

    }

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("OnTriggerEnter");
        Destroy(col.gameObject);
    }
}
