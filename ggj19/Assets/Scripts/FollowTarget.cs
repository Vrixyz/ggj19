﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform target;
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(transform.position.x - target.position.x) > 0.1f) {
            var distanceInX = target.position - transform.position;
            distanceInX.y = 0;
            distanceInX.z = 0;
            this.transform.Translate(distanceInX.normalized * speed * Time.deltaTime, Space.World);
        }
     }
}
