﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private bool hasBeenHit;
    private float timer;
    public int life;
    private float lifetime;

    // Start is called before the first frame update
    void Start()
    {
        hasBeenHit = false;
        timer = 0;
        lifetime = life / 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasBeenHit)
        {
            timer += Time.deltaTime;
            if (timer > lifetime)
            {
                Destroy(this.gameObject);
            }
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (!hasBeenHit)
        {
            hasBeenHit = true;
        }
    }    
}
