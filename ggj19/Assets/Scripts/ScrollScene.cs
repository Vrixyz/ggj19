﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollScene : MonoBehaviour
{
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        var children = gameObject.GetComponentsInDirectChildren<Transform>();

        foreach (Transform child in children)
            child.Translate(new Vector3(-speed * Time.deltaTime, 0, 0), Space.World);
    }
}
