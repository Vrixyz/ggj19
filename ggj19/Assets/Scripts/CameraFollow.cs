﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform objectToFollow;
    float cameraOffsetY;

    // Start is called before the first frame update
    void Start()
    {
        cameraOffsetY = objectToFollow.position.y - this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        var position = this.transform.position;
        position.y = objectToFollow.position.y - cameraOffsetY;
        this.transform.position = position;
    }
}
