﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreOnCollide : MonoBehaviour
{
    private ScoreManager scoreManager;
    public int point = 20;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.FindWithTag("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
            scoreManager.AddScore(point);
        }
    }
}
