﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Speed : IAction
{
    ScrollScene scrollScene;
    public float speedBoost = 2;
    public float speedTime = 1;
    public AudioClip shootSound;
    private AudioSource source;
    public IEnumerator FireAndUnfire(float countdownValue)
    {
        scrollScene.speed += speedBoost;
        yield return new WaitForSeconds(speedTime);
        scrollScene.speed -= speedBoost;
    }

    public override void Fire()
    {
        StartCoroutine(FireAndUnfire(speedTime));
        source.PlayOneShot(shootSound, 1);
    }

    // Start is called before the first frame update
    void Start()
    {
        source = this.GetComponent<AudioSource>();
        scrollScene = GameObject.Find("WorldScroll").GetComponent<ScrollScene>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}