﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBehavior : MonoBehaviour
{
    public float cooldown;
    public IAction action;

    float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = cooldown;
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(OnButtonPress);
    }

    public void OnButtonPress()
    {
        if (timer >= cooldown)
        {
            action.Fire();
            timer = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer < cooldown)
        {
            var percent = timer / cooldown;
            this.GetComponent<Image>().fillAmount = percent;
        }
        else
        {
            this.GetComponent<Image>().fillAmount = 1;
        }
    }
}
