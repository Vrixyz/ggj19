﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Fart : IAction
{
    public Vector3 jumpStrength = new Vector3(50f, 300, 0);
    public AudioClip shootSound;
    private AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = this.GetComponent<AudioSource>();
    }

    public override void Fire()
    {
        this.GetComponentInParent<Rigidbody>().AddForce(jumpStrength);
        source.PlayOneShot(shootSound, 1);
    }
}
